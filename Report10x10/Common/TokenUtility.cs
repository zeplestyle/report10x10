﻿using System;
using System.Web;

namespace Report10x10.Common
{
    /// <summary>
    /// 토큰을 관리하는 유틸리티 클래스 입니다.
    /// </summary>
    public class TokenUtility
    {
        private const string TOKEN_NAME = "__token";

        /// <summary>
        /// 토큰이 있는지 여부를 가져옵니다.
        /// </summary>
        public static bool HasToken
        {
            get { return HttpContext.Current.Request.Cookies[TOKEN_NAME] != null; }
        }

        /// <summary>
        /// 토큰을 가져옵니다.
        /// </summary>
        public static string Token
        {
            get { return HasToken ? HttpContext.Current.Request.Cookies[TOKEN_NAME].Value : null; }
        }

        /// <summary>
        /// 토큰이 발행되어 있지 않은 경우 새 토큰을 발행합니다.
        /// </summary>
        public static void GenerateToken()
        {
            if (!HasToken)
                HttpContext.Current.Response.Cookies.Set(new HttpCookie(TOKEN_NAME, Convert.ToString(Guid.NewGuid())));
        }
    }
}