﻿using Report10x10.Attributes;
using Report10x10.Common;
using Report10x10.Models.API;
using Report10x10.Models.DB;
using Report10x10.Repository;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;

namespace Report10x10.Controllers.API
{
    public class CartController : ApiController
    {
        /// <summary>
        /// 상품별 총 금액액 따른 적립 마일리지율을 반환합니다.
        /// </summary>
        /// <param name="price">상품 단가</param>
        /// <param name="qty">상품 구매수량</param>
        /// <returns>마일리지 적립율</returns>
        [NonAction]
        private float GetMileagePercent(int price, int qty)
        {
            return price * qty > 10000 ? 0.0025f : 0.001f;
        }

        /// <summary>
        /// 장바구니의 상품 목록을 가져옵니다.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [TokenRequire]
        public APIResponse<Cart> GetCart()
        {
            var data = DbHelper.GetDataFromProcedure<TBCart>("USP_GetCartItem", new System.Data.IDbDataParameter[]
            {
                new SqlParameter { ParameterName = "@UserToken", Value = TokenUtility.Token }
            });

            var cart = new Cart
            {
                TotalPrice = data.AsEnumerable().Sum(x => x.ProductQuantity * x.ProductPrice),
                Items = from row in data
                        select new Cart.CartItem
                        {
                            Seq = row.Seq,
                            ProductName = row.ProductName,
                            Quantity = row.ProductQuantity,
                            TotalPrice = row.ProductPrice * row.ProductQuantity,
                            Mileage = Math.Round(row.ProductPrice * row.ProductQuantity * GetMileagePercent(row.ProductPrice, row.ProductQuantity), 0, MidpointRounding.AwayFromZero),
                            ProductDescription = string.Format("마일리지 {0}% 적립", GetMileagePercent(row.ProductPrice, row.ProductQuantity) * 100)
                        }
            };

            return APIResponse<Cart>.OK(cart);
        }

        /// <summary>
        /// 장바구니에 상품을 추가합니다.
        /// </summary>
        /// <param name="item">추가할 상품 정보</param>
        /// <returns></returns>
        [HttpPost]
        [TokenRequire]
        public APIResponse<string> PostCart([FromBody] RequestPostCartItem item)
        {
            var result = DbHelper.ExecuteNonQueryFromProcedure("USP_SetCartItem", new System.Data.IDbDataParameter[] {
                new SqlParameter { ParameterName = "@UserToken", Value = TokenUtility.Token },
                new SqlParameter { ParameterName = "@ProductName", Value = item.Name },
                new SqlParameter { ParameterName = "@ProductQuantity", Value = item.Quantity },
                new SqlParameter { ParameterName = "@ProductPrice", Value = item.Price },
            });

            if (result == 1)
                return APIResponse<string>.OK("Y");

            return APIResponse<string>.FAIL("N");
        }

        /// <summary>
        /// 장바구니의 상품을 삭제합니다.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpDelete]
        [TokenRequire]
        public APIResponse<string> DeleteCart([FromBody] RequestDeleteCartItem item)
        {
            var result = DbHelper.ExecuteNonQueryFromProcedure("USP_DeleteCartItem", new System.Data.IDbDataParameter[] {
                new SqlParameter { ParameterName = "@UserToken", Value = TokenUtility.Token },
                new SqlParameter { ParameterName = "@Seq", Value = item.Seq }
            });

            if (result == 1)
                return APIResponse<string>.OK("Y");

            return APIResponse<string>.FAIL("N");
        }
    }
}
