﻿using Report10x10.Attributes;
using System.Web.Mvc;

namespace Report10x10.Controllers.View
{
    public class HomeController : Controller
    {
        /// <summary>
        /// 장바구니 화면
        /// </summary>
        /// <returns></returns>
        [TokenGenerate]
        public ActionResult Index()
        {
            return View();
        }
    }
}