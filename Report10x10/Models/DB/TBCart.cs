﻿namespace Report10x10.Models.DB
{
    /// <summary>
    /// DB TB_Cart 테이블 정보 입니다.
    /// </summary>
    public class TBCart
    {
        /// <summary>
        /// 고유번호
        /// </summary>
        public int Seq { get; set; }
        /// <summary>
        /// 상품명
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// 상품 단가
        /// </summary>
        public int ProductPrice { get; set; }
        /// <summary>
        /// 구매 수량
        /// </summary>
        public int ProductQuantity { get; set; }
    }
}