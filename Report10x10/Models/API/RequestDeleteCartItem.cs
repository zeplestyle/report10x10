﻿namespace Report10x10.Models.API
{
    /// <summary>
    /// 장바구니 삭제시 필요한 파라메터 입니다.
    /// </summary>
    public class RequestDeleteCartItem
    {
        /// <summary>
        /// 장바구니 고유번호를 가져오거나 설정합니다.
        /// </summary>
        public int Seq { get; set; }
    }
}