﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Report10x10.Models.API
{
    public class Cart
    {
        /// <summary>
        /// 장바구니 구매 총 금액을 가져오거나 설정합니다.
        /// </summary>
        [JsonProperty("total_price")]
        public int TotalPrice { get; set; }
        /// <summary>
        /// 장바구니 아이템 정보 목록을 가져오거나 설정합니다.
        /// </summary>
        [JsonProperty("cart_items")]
        public IEnumerable<CartItem> Items { get; set; }

        public class CartItem
        {
            /// <summary>
            /// 장바구니 고유번호를 가져오거나 설정합니다.
            /// </summary>
            [JsonProperty("seq")]
            public int Seq { get; set; }
            /// <summary>
            /// 상품명을 가져오거나 설정합니다.
            /// </summary>
            [JsonProperty("name")]
            public string ProductName { get; set; }
            /// <summary>
            /// 상품설명을 가져오거나 설정합니다.
            /// </summary>
            [JsonProperty("description")]
            public string ProductDescription { get; set; }
            /// <summary>
            /// 상품 수량을 가져오거나 설정합니다.
            /// </summary>
            [JsonProperty("quantity")]
            public int Quantity { get; set; }
            /// <summary>
            /// 상품 총 가격을 가져오거나 설정합니다.
            /// </summary>
            [JsonProperty("total_price")]
            public int TotalPrice { get; set; }
            /// <summary>
            /// 적립 마일리지를 가져오거나 설정합니다.
            /// </summary>
            [JsonProperty("mileage")]
            public double Mileage { get; set; }
        }
    }
}