﻿namespace Report10x10.Models.API
{
    /// <summary>
    /// 장바구니 입력시 필요한 파라메터 입니다.
    /// </summary>
    public class RequestPostCartItem
    {
        /// <summary>
        /// 상품명을 가져오거나 설정합니다.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 가격을 가져오거나 설정합니다.
        /// </summary>
        public int Price { get; set; }
        /// <summary>
        /// 수량을 가져오거나 설정합니다.
        /// </summary>
        public int Quantity { get; set; }
    }
}