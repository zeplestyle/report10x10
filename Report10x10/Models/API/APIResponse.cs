﻿using Newtonsoft.Json;

namespace Report10x10.Models.API
{
    /// <summary>
    /// API 처리결과 입니다.
    /// </summary>
    /// <typeparam name="T">메세지 형식</typeparam>
    public class APIResponse<T>
    {
        /// <summary>
        /// 응답결과 상태 코드를 가져오거나 설정합니다.
        /// </summary>
        [JsonProperty("response_code")]
        public int ResponseCode { get; set; }
        /// <summary>
        /// 응답결과 메세지를 가져오거나 설정합니다.
        /// </summary>
        [JsonProperty("response_message")]
        public string ResponseMessage { get; set; }
        /// <summary>
        /// 결과 데이터를 가져오거나 설정합니다.
        /// </summary>
        [JsonProperty("result")]
        public T Result { get; set; }

        private APIResponse() { }

        /// <summary>
        /// 응답성공 메세지를 생성합니다.
        /// </summary>
        /// <param name="data">전달할 결과 데이터</param>
        /// <returns></returns>
        public static APIResponse<T> OK(T data)
        {
            return new APIResponse<T>
            {
                ResponseCode = 0,
                ResponseMessage = string.Empty,
                Result = data
            };
        }

        /// <summary>
        /// 응답실패 메세지를 생성합니다.
        /// </summary>
        /// <param name="message">요청 처리실패 사유</param>
        /// <returns></returns>
        public static APIResponse<T> FAIL(string message)
        {
            return new APIResponse<T>
            {
                ResponseCode = -1,
                ResponseMessage = message,
                Result = default(T)
            };
        }
    }
}