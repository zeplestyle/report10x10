﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Report10x10.Repository
{
    /// <summary>
    /// DB 유틸리티 클래스 입니다.
    /// </summary>
    public class DbHelper
    {
        #region Private

        /// <summary>
        /// DBContext를 생성하고 반환합니다.
        /// </summary>
        /// <returns>DBContext</returns>
        public static DbContext GetDbContext()
        {
            return new DbContext(ConfigurationManager.ConnectionStrings["DB_CART"].ConnectionString);
        }

        /// <summary>
        /// 프로시저명과 파라메터를 조합하여 SQL 실행문을 생성하여 반환합니다.
        /// </summary>
        /// <param name="procedureName">프로시저명</param>
        /// <param name="parameters">프로시저 파라메터</param>
        /// <returns>실행 쿼리문</returns>
        private static string GenerateProcedureQuery(string procedureName, IDbDataParameter[] parameters)
        {
            var parameter = parameters.Select(p => {
                if (p.Value == null)
                    p.Value = DBNull.Value;

                return p.ParameterName + "=" + p.ParameterName + (IsOutputParameter(p) ? " OUTPUT" : string.Empty);
            }).Aggregate((prev, next) => prev + ", " + next);
            return string.Format("exec {0} {1}", procedureName.Trim(), parameter).Trim();
        }

        /// <summary>
        /// 출력용 파라메터인지 여부를 반환합니다.
        /// </summary>
        /// <param name="parameter">파라메터</param>
        /// <returns>출력용 파라메터 여부</returns>
        private static bool IsOutputParameter(IDbDataParameter parameter)
        {
            return parameter.Direction == ParameterDirection.Output || parameter.Direction == ParameterDirection.InputOutput;
        }

        #endregion

        #region ORM

        /// <summary>
        /// 프로시저를 수행하고 결과의 첫번째 Row를 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환형식</typeparam>
        /// <param name="procedure">프로시저명</param>
        /// <param name="parameters">SQL 파라메터</param>
        /// <returns>수행 결과</returns>
        public static T GetSingleDataFromProcedure<T>(string procedure, params IDbDataParameter[] parameters)
        {
            return GetData<T>(GenerateProcedureQuery(procedure, parameters), parameters).FirstOrDefault();
        }

        /// <summary>
        /// 프로시저를 수행하고 결과를 컬렉션으로 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환형식</typeparam>
        /// <param name="procedure">프로시저명</param>
        /// <param name="parameters">SQL 파라메터</param>
        /// <returns>수행 결과</returns>
        public static IEnumerable<T> GetDataFromProcedure<T>(string procedure, params IDbDataParameter[] parameters)
        {
            return GetData<T>(GenerateProcedureQuery(procedure, parameters), parameters);
        }

        /// <summary>
        /// SQL 쿼리문을 수행하고 결과의 첫번째 Row를 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환형식</typeparam>
        /// <param name="query">SQL 쿼리문</param>
        /// <param name="parameters">SQL 파라메터</param>
        /// <returns>수행 결과</returns>
        public static T GetSingleData<T>(string query, params IDbDataParameter[] parameters)
        {
            return GetData<T>(query, parameters).FirstOrDefault();
        }

        /// <summary>
        /// SQL 쿼리문을 수행하고 결과를 컬렉션으로 반환합니다.
        /// </summary>
        /// <typeparam name="T">반환형식</typeparam>
        /// <param name="query">SQL 쿼리문</param>
        /// <param name="parameters">SQL 파라메터</param>
        /// <returns>수행 결과</returns>
        public static IEnumerable<T> GetData<T>(string query, params IDbDataParameter[] parameters)
        {
            IEnumerable<T> result;

            using (var db = GetDbContext())
            {
                result = db.Database.SqlQuery<T>(query, parameters).ToList();
            }

            return result;
        }

        /// <summary>
        /// 다중 결과셋을 반환하는 프로시저를 수행합니다.
        /// </summary>
        /// <param name="query">프로시저명</param>
        /// <param name="parameters">SQL 파라메터</param>
        /// <param name="action">결과셋에 대한 처리</param>
        public static void GetDataFromProcedure(string procedure, IDbDataParameter[] parameters, System.Action<int, MultipleResultHelper> action)
        {
            GetData(GenerateProcedureQuery(procedure, parameters), parameters, action);
        }

        /// <summary>
        /// 다중 결과셋을 반환하는 SQL 쿼리문을 수행합니다.
        /// </summary>
        /// <param name="query">SQL 쿼리문</param>
        /// <param name="parameters">SQL 파라메터</param>
        /// <param name="action">결과셋에 대한 처리</param>
        public static void GetData(string query, IDbDataParameter[] parameters, System.Action<int, MultipleResultHelper> action)
        {
            using (var db = GetDbContext())
            {
                using (var connection = db.Database.Connection)
                {
                    connection.Open();
                    var command = connection.CreateCommand();
                    command.CommandText = query;
                    command.Parameters.AddRange(parameters);

                    using (var reader = command.ExecuteReader())
                    {
                        var adapter = ((IObjectContextAdapter)db);
                        var idx = 0;

                        do
                        {
                            action(idx, new MultipleResultHelper { adapter = adapter, data = reader });
                            idx++;
                        } while (reader.NextResult());
                    }
                }
            }
        }

        #endregion

        #region ExecuteNonQuery

        /// <summary>
        /// SQL 쿼리문을 수행하고 적용된 행 수를 반환합니다.
        /// </summary>
        /// <param name="procedure">프로시저 명</param>
        /// <param name="parameters">SQL 파라메터</param>
        /// <returns>영향을 받은 행 수</returns>
        public static int ExecuteNonQueryFromProcedure(string procedure, IDbDataParameter[] parameters)
        {
            return ExecuteNonQuery(GenerateProcedureQuery(procedure, parameters), parameters);
        }

        /// <summary>
        /// SQL 쿼리문을 수행하고 적용된 행 수를 반환합니다.
        /// </summary>
        /// <param name="query">SQL 쿼리문</param>
        /// <param name="parameters">SQL 파라메터</param>
        /// <returns>영향을 받은 행 수</returns>
        public static int ExecuteNonQuery(string query, IDbDataParameter[] parameters)
        {
            var retVal = 0;

            using (var db = GetDbContext())
                retVal = db.Database.ExecuteSqlCommand(query, parameters);

            return retVal;
        }

        #endregion

        /// <summary>
        /// 파라메터의 값을 가져옵니다.
        /// </summary>
        /// <typeparam name="T">가져올 형식</typeparam>
        /// <param name="parameters">찾을 파라메터</param>
        /// <param name="parameterName">파라메터 이름</param>
        /// <returns>값</returns>
        public static T GetOutputValue<T>(IDbDataParameter[] parameters, string parameterName)
        {
            var result = from x in parameters.AsQueryable()
                         where x.ParameterName.Equals(parameterName, System.StringComparison.OrdinalIgnoreCase)
                         select x.Value;

            return result.Cast<T>().FirstOrDefault();
        }
    }
}