﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Report10x10.Repository
{
    public class MultipleResultHelper
    {
        internal IObjectContextAdapter adapter { get; set; }
        internal DbDataReader data { get; set; }

        /// <summary>
        /// 결과 집합을 리스트 형식으로 가져옵니다.
        /// </summary>
        /// <typeparam name="T">가져올 형식</typeparam>
        /// <returns></returns>
        public IEnumerable<T> GetList<T>()
        {
            return adapter.ObjectContext.Translate<T>(data).ToList();
        }

        /// <summary>
        /// 결과 집합을 가져옵니다.
        /// </summary>
        /// <typeparam name="T">가져올 형식</typeparam>
        /// <returns></returns>
        public T GetData<T>()
        {
            return adapter.ObjectContext.Translate<T>(data).FirstOrDefault();
        }
    }
}