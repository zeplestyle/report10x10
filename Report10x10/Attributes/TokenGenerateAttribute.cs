﻿using Report10x10.Common;
using System;
using System.Web.Mvc;

namespace Report10x10.Attributes
{
    /// <summary>
    /// 토큰을 확인하고 신규 발생하는 속성입니다.
    /// 메서드 또는 클래스에 지정할 수 있습니다.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class TokenGenerateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            TokenUtility.GenerateToken();
        }
    }
}