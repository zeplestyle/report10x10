﻿using Report10x10.Common;
using Report10x10.Models.API;
using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Report10x10.Attributes
{
    /// <summary>
    /// 토큰 유효성 여부를 체크하는 속성입니다.
    /// 메서드 또는 클래스에 지정할 수 있습니다.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public class TokenRequireAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!TokenUtility.HasToken)
            {
                actionContext.Response = actionContext.Request.CreateResponse(APIResponse<string>.FAIL("사용자를 확인할 수 없습니다."));
                return;
            }
        }
    }
}